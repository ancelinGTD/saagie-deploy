#!/bin/bash

## define partitions that must be created
PARTS='tmp var mesos opt'
PARTS=$PARTS' hdfs' # hdfs must be defined last as it muse use all the remaining space !

PARTS_tmp_path='/tmp'
PARTS_tmp_size='30'
PARTS_tmp_opts='defaults,noatime,nodev,nosuid'

PARTS_var_path='/var'
PARTS_var_size='60'
PARTS_var_opts='defaults,nosuid'

PARTS_mesos_path='/mesos'
PARTS_mesos_size='50'
PARTS_mesos_opts='defaults,noatime,nodev,nosuid'

PARTS_opt_path='/opt'
PARTS_opt_size='60'
PARTS_opt_opts='defaults,noatime,nodev,nosuid'

PARTS_hdfs_path='/data'
PARTS_hdfs_size=''
PARTS_hdfs_opts='defaults,noatime,nodev,nosuid'

to_reboot=False

## disks devices listing, skipping sda and sdb
DEVS=$(ls -1 /dev/sd* | egrep -v 'sda|sdb|[0-9]')
for DEV in $DEVS; do
    DISK=$(partx -s $DEV 2>&1)
    echo $DISK | egrep "failed to read partition table"
    r=$?
    if [ $r -eq 0 ]; then
        logger -s $DEV" will be used for partitioning" 2> /dev/kmsg

        ## create an extended partition on the whole disk
        echo -e 'o\nn\ne\n\n\n\nw\n' | fdisk $DEV > /dev/kmsg 2>&1
        [ $? -ne 0 ] && logger -s 'Failed to create extended partition on '$DEV 2> /dev/kmsg

        ## create logical partition for each defined partitions
        for part in $PARTS; do
            size="PARTS_"$part"_size" && size=${!size}
            [ -n "$size" ] && size="+"$size"G"
            path="PARTS_"$part"_path" && path=${!path}
            opts="PARTS_"$part"_opts" && opts=${!opts}

            echo -e "n\nl\n\n$size\nw\n" | fdisk $DEV  > /dev/kmsg 2>&1
            
            partprobe $DEV > /dev/kmsg 2>&1
            
            ## file system creation
            partnum=$(fdisk $DEV -l|tail -n1|awk -e '{ print $1 }')    
            [ $? -ne 0 ] && logger -s 'Failed to get partnum' 2> /dev/kmsg && continue
            mkfs.ext4 $partnum
            [ $? -ne 0 ] && logger "Failed to create $partnum filesystem" 2> /dev/kmsg && continue
            
            ## mkfs succeeded, but fsck to be sure
            fsck -N $partnum|grep "ext4"
            [ $? -ne 0 ] && logger "fsck failed for $partnum" 2> /dev/kmsg

            ## remove the 5% reserved for root
            [[ "$part" == 'mesos' || "$part" == 'hdfs' ]] && tune2fs -m 0 $partnum

            ## get blkid of partition          
            blk=$(blkid $partnum|awk -e '{print $2}'|sed -e 's/"//g')
            echo $blk

            ## handle existing data where the partition must be mounted
            if [ -e $path ]; then
                # copy data in a temporary mountpoint
                temporary=/mnt/part$RANDOM
                mkdir $temporary
                mount $partnum $temporary
                cp -pr $path/* $temporary
                umount $temporary
                rmdir $temporary
            fi

            ## populate fstab and mountpoints
            mkdir $path
            echo "$blk $path ext4 $opts 0 0" >> /etc/fstab

            ## fix mountpoint permissions after reboot
            [ "$part" == "tmp" ] && echo "@reboot root chmod 1777 $path" >> /etc/cron.d/mountpermissions
            [ "$part" == "hdfs" ] && echo "@reboot root chmod 700 $path" >> /etc/cron.d/mountpermissions
            
            to_reboot=True
        done		
    fi
done

if [ $to_reboot == True ]; then
    touch /tmp/must_reboot
fi
exit 0